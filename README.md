CC-Tan

Autori: Gianmarco Magnani, Nicolas Pasolini, Lorenzo Sutera, Paolo Baldini

Il giocatore vestirà i panni di una navicella nello spazio fissa al centro che dovrà abbattere i blocchi provenienti da tutte le direzioni prima che questi la raggiungano. I blocchi verranno generati con una determinata vita (numero di colpi che deve subire per essere distrutto). Lo scopo del gioco è quello di accumulare quanti più punti possibili stabilendo di volta in volta nuovi record personali.